/**
 * Gimcrack
 *
 * A basic tooltip plugin
 *
 * Version 0.2
 */

;(function( $, window, document, undefined )
{
	var pluginName = 'gimcrack';
		
	function GimCrack( element, options )
	{
		var base = this;
		
		base.$el = $(element);
		base.el  = element;
		
		base.options   = $.extend( {}, base.defaults, options);
		
		base._defaults = base.defaults;
		base._name     = pluginName;
		
		base.init();
	}
	
	/**
	 * Initialize
	 */
	GimCrack.prototype = {
		defaults: {
			speed: 200,
			delay: 300,
			addClass: '',
			slideOffset: 20,
			bgcolor: 'ab0000-e41616',
			color: 'ffffff',
			offset: 0,
			pointer: true
		},
		init: function()
		{
			var base = this;
			
			base.addToolTip();
			
			base.activateElement();
		},
		
		/**
		 * Delay the fade-in animation of the tooltip
		 */
		activateElement: function()
		{
			var base = this;
			
			// Get the title attribute to use for tooltip
			base.options.el_text = base.$el.attr('title');
			// empty the title attribute, so it won't compete with new tooltip
			base.$el.attr('title', "");
			
			base.$el.hover(
				function() {
					base.setTip();
				},
				function() {
					base.$el.removeClass('active');
					base.stopTimer();
					base.$tooltip.hide();
				}
			);
		},
		
		/**
		 * 
		 */
		forceShow: function()
		{
			var base = this;
			
			base.setTip();
			base.showTip();
		},
		
		/**
		 * Delay the fade-in animation of the tooltip
		 */
		setTimer: function()
		{
			var base = this;
			
			base.options.timer = setInterval( function() { base.showTip(); }, base.options.delay );
		},
		
		/**
		 * Delay the fade-in animation of the tooltip
		 */
		stopTimer: function()
		{
			clearInterval( this.options.timer );
		},
		
		/**
		 * Position the tooltip relative to the element
		 */
		setTip: function()
		{
			var base = this;
			
			if (! base.$el.is('.active') )
			{
				$('.content', base.$tooltip).html("").html(base.options.el_text);
				
				var el_offset  = base.$el.offset();
				var el_left    = el_offset.left;
				var el_top     = el_offset.top;
				var el_width   = base.$el.width();
				//var el_height  = base.$el.height();
				
				var tip_height = base.$tooltip.outerHeight(true);
				var tip_width  = base.$tooltip.outerWidth(true);
				var tip_x      = (el_left - (tip_width - el_width)/2) + base.options.offset + "px";
				
				var tip_y      = el_top - tip_height - base.options.slideOffset + "px";
				
				base.$tooltip.css({ top: tip_y, left: tip_x, opacity: 0.0 });
				
				if ( base.options.addClass ) { base.$tooltip.addClass(base.options.addClass); }
				
				base.$el.addClass('active');
				
				base.setTimer();
			}
		},
		
		/**
		 * This function stops the timer and creates the fade-in animation
		 */
		showTip: function()
		{
			var base = this;
			
			base.stopTimer();
			base.$tooltip
				.show()
				.animate({ top: "+=" + base.options.slideOffset + "px", opacity: 1.0 }, base.options.speed);
		},
		
		/**
		 * Add the tip to the page
		 */
		addToolTip: function()
		{
			var base = this,
				$tooltip;
			
			if ( $('body > #' + pluginName).length > 0 )
			{
				$tooltip = $('body > #' + pluginName);
			}
			else
			{
				$tooltip = $('<div />', { 'id': pluginName });
				$tooltip
					.append( $('<div />', { 'class': 'content' }) )
					.hide();
				
				if ( base.options.pointer )
				{
					$tooltip
						.append( $('<span />') );
				}
				
				$("body").prepend($tooltip);
			}
			
			// Add some basic styles here to keep this one concise library
			// Setup background color
			var bg = base.options.bgcolor.split('-');
			if ( bg[1] )
			{
				$tooltip
					.css('background','#'+ bg[0])
					.css('background','-webkit-gradient(linear, 0% 0%, 0% 100%, from(#'+bg[0]+'), to(#'+bg[1]+'))')
					.css('background','-webkit-linear-gradient(top, #'+bg[1]+', #'+bg[0]+')')
					.css('background','-moz-linear-gradient(top, #'+bg[1]+', #'+bg[0]+')')
					.css('background','-ms-linear-gradient(top, #'+bg[1]+', #'+bg[0]+')')
					.css('background','-o-linear-gradient(top, #'+bg[1]+', #'+bg[0]+')')
			}
			else
			{
				$tooltip
					.css('background','#'+ bg[0]);
			}
			
			if ( base.options.width )
			{
				$tooltip
					.css({'width': base.options.width +'px'});
			}
			
			$tooltip
				.css({
					'color': '#'+ base.options.color,
					'margin-bottom': '5px',
					'padding': '.5em .8em',
					'position': 'absolute',
					'z-index': '5000',
					'border-radius': '5px',
					'-moz-border-radius': '5px',
					'-webkit-border-radius': '5px',
					'-khtml-border-radius': '5px'
				})
				.hide();
			
			$('span', $tooltip)
				.css({
					'border-left': '5px solid transparent',
					'border-right': '5px solid transparent',
					'border-top': '5px solid #'+ bg[0],
					'display': 'block',
					'position': 'absolute',
					'left': '49%',
					'bottom': '-5px',
					'width': '0',
					'height': '0',
					'z-index': '200'
				});
			
			base.$tooltip = $tooltip;
			
			$(window).scroll(function() {
				base.$tooltip.hide();
			});
		}
	};
	
	$.fn[pluginName] = function( options )
	{
		return this.each(function()
		{
			if (! $.data(this, 'plugin_' + pluginName) )
			{
				$.data(this, 'plugin_' + pluginName,
				new GimCrack( this, options ));
			}
		});
	};

})( jQuery, window, document );